Platform: Windows Universal app | Android | iOS (maybe) 
 
Purpose: To provide easy entry of shared expenses and view of how much each person owes 
 
Features: 
- To make use of contact list to enter friend's name and allow one click button to SMS or email to the people 
- To allow convenient view of current status (who has paid and not) + send reminders 
- To allow convenient view of all the activities and expenses spent 
- Synchronization across all devices using cloud storage (either OneDrive or Google Drive) and secure 3rd party login system (Facebook, Microsoft, Google) 
- ***Share & sync the activities with other users so they can view it on their device 
 
Monetization: Ads | One-time payment ($0.99 or $1.99) | In-app purchases of premium features (removal of Ads) 
 
Target number of users each time: | 2 to 20 
 
Notes: 
- A little social since its bill SHARING  
- User need not login - synchronization is optional, in other words, user can use the app offline and the activities will be saved on device's local storage 
- User can choose "when" to synchronize